from setuptools import setup

setup(name='sentimex',
      version='0.1',
      description='The sentiments from Google News',
      url='https://bitbucket.org/osenturk/textanalytics-python-twitter/src/master/',
      author='Ozan Senturk',
      author_email='ozan.senturk@gmail.com',
      license='MIT',
      packages=['utils'],
      zip_safe=False)