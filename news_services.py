""" Develop an small web API with two endpoints to:
    get a JSON object with the available files_dir
    and get a JSON object with archived files_dir."""

from flask import Flask, jsonify, render_template

from flask_httpauth import HTTPBasicAuth
from flask import make_response

from utils.collect_news import save_news
from utils.tweet_utils import load_config
from utils.dao_layer import select_all
from utils.collect_news import export_to_csv
from utils.collect_news import add_sentiment

app = Flask(__name__)

auth = HTTPBasicAuth()


@auth.get_password
def get_password(username):
    if username == 'python':
        return 'pass'
    return None


@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized access'}), 401)


@app.route('/api/get_all', methods=['GET'])
# @auth.login_required
def get_all():

    cfg = load_config()
    # news were fetched and stored into the database
    save_news(cfg, False)
    # all news were listed in raw format
    news_list = select_all(cfg, True)

    # adding sentiments
    add_sentiment(cfg, news_list)

    # all news were listed in raw format
    sent_list = select_all(cfg, True)

    sent_list = sorted(sent_list,
                       key=lambda tweet: (tweet.keyword, -tweet.p_desc))

    export_to_csv(sent_list, cfg)
    return render_template('news.html', news_list=sent_list)


@app.route('/api/get_by_sources', methods=['GET'])
# @auth.login_required
def get_by_sources():

    cfg = load_config()
    # news were fetched and stored into the database
    save_news(cfg, True)
    # all news were listed in raw format
    news_list = select_all(cfg, True)

    # adding sentiments
    add_sentiment(cfg, news_list)

    # all news were listed in raw format
    sent_list = select_all(cfg, True)

    sent_list = filter(lambda member: member.keyword is not None, sent_list)

    sent_list = sorted(sent_list,
                       key=lambda tweet: (tweet.keyword, -tweet.p_desc))

    export_to_csv(sent_list, cfg)
    return render_template('news.html', news_list=sent_list)

@app.route("/", methods=["GET"])
def index():
    """
        renders the home page

    :return:
    """

    return render_template("index.html")


if __name__ == "__main__":

    import logging
    import logging.config

    logging.config.fileConfig('logging.conf')



    app.run(debug=True, host="0.0.0.0", port=4000)
