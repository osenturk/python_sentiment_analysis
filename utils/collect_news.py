""" gets Google News for each keyword, aggregates the results in a data frame,
    thereafter creates a file named with the date 28-04-2019"""

import json
from datetime import datetime, timedelta
import urllib.parse
import csv
import requests
import pandas as pd


from sqlalchemy_utils import database_exists

from utils.tweet_utils import json_news_to_object
from utils.dao_layer import initialize_database
from utils.dao_layer import save_all
from utils.dao_layer import delete_all
from utils.tweet_utils import sort_tweet
from utils.tweet_utils import get_sources
from utils.tweet_utils import get_sentiment


COLUMN_NAMES = [
    "author",
    "title",
    "description",
    "url",
    "image",
    "s-title",
    "s-description",
    "s-avg",
    "keyword",
    "news_date",
]

article_frame = pd.DataFrame(data=None, index=None, columns=COLUMN_NAMES)
# api.PostUpdate('A test message with twitter API')


def get_news(s_date, e_date, keyword, cfg):
    """ fetch the news from Google News API """

    encoded_keyword = urllib.parse.quote(keyword)

    url = (
        cfg["google_news"]["api_url"]
        + "q="
        + encoded_keyword
        + "&from="
        + s_date
        + "&to="
        + e_date
        + "&pageSize="
        + cfg["google_news"]["criteria"]["page_size"]
        + "&page="
        + cfg["google_news"]["criteria"]["page_count"]
        + "&language="
        + cfg["google_news"]["criteria"]["language"]
        + "&sortBy="
        + cfg["google_news"]["criteria"]["sort_by"]
        + "&apiKey="
        + cfg["google_news"]["api_key"]
    )

    response = requests.get(url)

    print(json.dumps(response.json(), sort_keys=True, indent=4, separators=(",", ": ")))

    articles_json = response.json()
    if response.status_code != 200:
        print("response message is: " + articles_json["message"])
        articles = []
    else:
        articles = articles_json["articles"]

    return articles


def get_news_by_sources(s_date, e_date, keyword, cfg, sources):
    """ fetch the news from Google News API """

    encoded_keyword = urllib.parse.quote(keyword)

    url = (
        cfg["google_news"]["api_url"]
        + "q="
        + encoded_keyword
        + "&sources="
        + sources
        + "&from="
        + s_date
        + "&to="
        + e_date
        + "&pageSize="
        + cfg["google_news"]["criteria"]["page_size"]
        + "&page="
        + cfg["google_news"]["criteria"]["page_count"]
        + "&language="
        + cfg["google_news"]["criteria"]["language"]
        + "&sortBy="
        + cfg["google_news"]["criteria"]["sort_by"]
        + "&apiKey="
        + cfg["google_news"]["api_key"]
    )

    response = requests.get(url)

    print(json.dumps(response.json(), sort_keys=True, indent=4, separators=(",", ": ")))

    articles_json = response.json()
    if response.status_code != 200:
        print("response message is: " + articles_json["message"])
        articles = []
    else:
        articles = articles_json["articles"]

    return articles


def save_news(cfg, is_all_sources):

    if database_exists(cfg["sqlite"]["engine"]["url"]) == False:
        initialize_database(cfg)
    else:
        delete_all(cfg)

    last_date = str(datetime.now())[0:10]
    first_date = str(
        datetime.today() - timedelta(days=cfg["google_news"]["criteria"]["news_age"])
    )[0:10]

    keywords = cfg["google_news"]["keywords"]
    sources = get_sources(cfg)
    limit = cfg["rest"]["news_limit"]

    for keyword in keywords:

        if is_all_sources == True:
            google_news = get_news_by_sources(
                first_date, last_date, keyword, cfg, sources
            )
        else:
            google_news = get_news(first_date, last_date, keyword, cfg)

        df_news = pd.DataFrame(google_news)
        df_news.dropna(inplace=True)
        df_news.drop_duplicates(subset='title', keep='first',inplace=True)
        google_news = df_news.to_dict(orient='records')

        tweet_news = [json_news_to_object(g_news, keyword) for g_news in google_news]

        tweet_news = filter_google_news(tweet_news, limit)

        save_all(tweet_news, cfg)


def filter_google_news(google_news, limit):

    google_news.sort(key=sort_tweet, reverse=True)

    return google_news[0:limit]


def export_to_csv(tweets, cfg):

    tweets_dict = [vars(tweet) for tweet in tweets]
    filename = cfg["output"]["name"]

    keys = tweets_dict[0].keys()
    with open(filename, "w", encoding="utf-8") as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(tweets_dict)


def add_sentiment(cfg, tweets):

    sentiment_list = []

    for tweet in tweets:
        tweet.p_title = get_sentiment(tweet.title)
        tweet.p_desc = get_sentiment(tweet.desc)
        sentiment_list.append(tweet)

    delete_all(cfg)
    save_all(sentiment_list, cfg)


# cfg = load_config()
#
# tweets = select_all(cfg, True)
#
# add_sentiment(cfg,tweets)
#
# tweeets = select_all(cfg, True)
