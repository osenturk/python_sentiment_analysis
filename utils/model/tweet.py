class Tweet:

    # Initializer / Instance Attributes
    def __init__(self, author, title, desc, url, image, 
    s_title, s_desc, keyword, source, p_title, p_desc, news_date):

        self.author = author
        self.title = title
        self.desc = desc  # time of last status change
        self.url = url  # time of last modification
        self.image = image
        self.s_title = s_title
        self.s_desc = s_desc
        self.keyword = keyword
        self.source = source
        self.p_title = p_title
        self.p_desc = p_desc
        self.news_date = news_date

    def __repr__(self):
        return "<YourFile( author='%s', title='%s', description='%s', url='%s', " \
               "image='%s', s_title='%f, s_desc='%f, keyword='%s, source='%s', " \
               "p_title='%f', p_desc='%f', news_date='%s')>" % (
            self.author, self.title, self.desc, self.url,
            self.image, self.s_title, self.s_desc, self.keyword, 
            self.source, self.p_title, self.p_desc, self.news_date)