""" persistance layer """


from sqlalchemy import create_engine
from sqlalchemy_utils import create_database
from sqlalchemy import Table, Column, Integer, String, Float, DateTime
from sqlalchemy.sql import select
from sqlalchemy import MetaData
from sqlalchemy import inspect

from utils.tweet_utils import result_to_tweet


def get_engine(cfg):

    engine_url = cfg['sqlite']['engine']['url']
    engine = create_engine(engine_url)

    return engine

def get_connection(get_engine):

    engine = get_engine

    connection = engine.connect()

    return connection

def initialize_database(cfg):

    create_database(cfg['sqlite']['engine']['url'])

    engine = get_engine(cfg)
    metadata = MetaData()

    Table('TWEETS', metadata,

                   Column('id', Integer, primary_key=True),

                   Column('author', String),

                   Column('title', String),

                   Column('desc', String),

                   Column('url', String),

                   Column('image', String),

                   Column('s_title', Float),

                   Column('s_desc', Float),

                   Column('keyword', String),

                   Column('source', String),

                   Column('p_title', Float),

                   Column('p_desc', Float),
                   Column('news_date', String)
                   )

    metadata.create_all(engine)


def save_all(tweets, cfg):
    """ testing bulk inserts """

    tweet_dict = [vars(each_tweet) for each_tweet in tweets]

    engine = get_engine(cfg)

    # Create MetaData instance
    meta = MetaData()
    meta.reflect(bind=engine)

    files_table = meta.tables['TWEETS']

    conn = engine.connect()

    # insert multiple data
    conn.execute(files_table.insert(), tweet_dict)


def select_all(cfg, is_object):

    engine = get_engine(cfg)

    meta = MetaData()
    meta.reflect(bind=engine)

    inspector = inspect(engine)

    table_list = inspector.get_table_names()

    if 'TWEETS' in table_list:
        table = meta.tables['TWEETS']

        select_st = select([table])


        conn = engine.connect()
        res = conn.execute(select_st)

        if(is_object == True):
            tweets = [result_to_tweet(one_file) for one_file in res]
        else:
            tweets = [one_file for one_file in res]

    else:
        tweets = []

    return tweets


def delete_all(cfg):

    engine = get_engine(cfg)

    meta = MetaData()
    meta.reflect(bind=engine)
    tweets = meta.tables['TWEETS']

    del_st = tweets.delete()

    engine.connect().execute(del_st)


