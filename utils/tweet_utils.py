""" tweet utils"""

import yaml
import sys

import nltk
import logging

from nltk.sentiment.vader import SentimentIntensityAnalyzer
from utils.model.tweet import Tweet

logger = logging.getLogger(__name__)

def load_config():

    CONFIG_PATH = "config.yaml"

    with open(CONFIG_PATH, 'r') as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    return cfg


# def find_similarities(text1, keyword, NLP):
#     """ find similarities between keyword and news """
#
#     # Text analytics
#
#     similarity = 0
#     if text1 is not None:
#
#         try:
#             # print('text1: '+ text1)
#             # print('keyword: ' + keyword)
#
#             doc1 = NLP(u"" + text1)
#             doc2 = NLP(u"" + keyword)
#             similarity = doc1.similarity(doc2)
#
#         except TypeError as type_error:
#             print(type_error)
#             print("inputs must be text: "+text1)
#     else:
#         print("either title or description is empty for the keyword: "+ keyword)
#
#     return similarity


def harmonic_mean(s_title, s_desc):

    if (s_title + s_desc) != 0:
        h_mean = (2 * (s_title * s_desc)) / (s_title + s_desc)
    else:
        h_mean = 0

    return h_mean


def json_news_to_object(article, keyword, s_title=0, s_desc=0, p_title=0, p_desc=0):
    """ news JSON to Object conversion """

    # s_title = find_similarities(article['title'], keyword)
    # s_desc = find_similarities(article['description'], keyword)


    tweet = Tweet(article['author'], article['title'], article['description'], article['url'],
                  article['urlToImage'], s_title, s_desc,
                  keyword,article['source']['name'], p_title, p_desc, article['publishedAt'][:10])

    return tweet


def result_to_tweet(result):
    """ news JSON to Object conversion """

    tweet = Tweet(result['author'], result['title'], result['desc'], result['url'],
                  result['image'], result['s_title'], result['s_desc'],
                  result['keyword'], result['source'],result['p_title'], result['p_desc'], 
                  result['news_date'])

    return tweet

def create_tweet_class():

    article = {"author": "Ozan", "title": "title", "description": "desc",
               "url": "www.ozansenturk.com", "urlToImage": "sentrworks", 
               "source":  {"name":"bloomberg"}, "publishedAt":"2019-06-03"}

    tweet = json_news_to_object(article, 'keyword')

    return tweet


""" decides the sort based on which column"""
def sort_tweet(tweet):
    return tweet.p_desc


def get_sources(cfg):

    source_list = cfg['google_news']['sources']
    sources = ",".join(source_list)

    logger.debug(sources)

    return sources

class Singleton:
   __instance = None
   @staticmethod
   def getInstance():
      """ Static access method. """
      if Singleton.__instance == None:
         Singleton()
      return Singleton.__instance

   def __init__(self):
      """ Virtually private constructor. """
      if Singleton.__instance != None:
         raise Exception("This class is a singleton!")
      else:
         nltk.download('vader_lexicon')
         print("downloading vade_lexicon")
         Singleton.__instance = self

def get_sentiment(text):

    if text is not None:
        sentiment_analyser = SentimentIntensityAnalyzer()
        ss = sentiment_analyser.polarity_scores(text)
        return ss['compound']

    else:

        return 0
