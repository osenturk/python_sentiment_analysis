""" test configuration """

import yaml
import pytest

from sqlalchemy import create_engine
from sqlalchemy_utils import drop_database, create_database
from sqlalchemy import Table, Column, Integer, String, MetaData, DateTime, Boolean, Float

@pytest.fixture
def get_config():

    workspace = '/Users/ozansenturk/PycharmProjects/TwitterMe/'
    config_path = workspace + 'config.yaml'
    with open(config_path, 'r') as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    return cfg


@pytest.fixture
def get_engine(get_config):

    engine_url = get_config['sqlite']['engine']['url']
    engine = create_engine(engine_url)

    return engine


@pytest.fixture
def get_connection(request, get_engine):

    # dialect + driver: // username: password @ host:port / database
    engine = get_engine
    connection = engine.connect()

    def fin():
        print("teardown connection")
        connection.close()

    request.addfinalizer(fin)

    return connection


@pytest.fixture
def create_table(request, get_engine):

    create_database(get_engine.url)

    metadata = MetaData()
    tweets = Table('TWEETS', metadata,

        Column('id', Integer, primary_key=True),

        Column('author', String),

        Column('title', String),

        Column('desc', String),

        Column('url', String),

        Column('image', String),

        Column('s_title', Float),

        Column('s_desc', Float),

        Column('keyword', String),

        Column('source', String),

       Column('p_title', Float),

       Column('p_desc', Float),

        Column('news_date', String),

       )

    metadata.create_all(get_engine)

    def fin():
        print("teardown connection")
        drop_database(get_engine.url)

    request.addfinalizer(fin)

    return tweets


@pytest.fixture
def create_insert(request, create_table):

    tweets = create_table

    ins = tweets.insert().values(author='Ozan', title='people', desc='desc', url='url',
                                 image='image', s_title=0.99, s_desc=0.99,
                                 keyword='keyword', source='bloomberg',p_title=0.33,p_desc=0.55,news_date='2019-06-03')

    return ins, tweets
