""" testing persistance layer """

import sqlalchemy
import csv
import pandas as pd

from sqlalchemy_utils import database_exists, drop_database, create_database
from sqlalchemy.sql import select
from sqlalchemy import update

from utils.tweet_utils import create_tweet_class
from utils.dao_layer import select_all


print("corpus was loaded...")

def test_create_db(get_config):

    engine_url = get_config['sqlite']['engine']['url']

    create_database(engine_url)

    assert database_exists(engine_url) == True

    drop_database(engine_url)


def test_drop_db(get_config):

    engine_url = get_config['sqlite']['engine']['url']

    create_database(engine_url)

    drop_database(engine_url)

    assert database_exists(engine_url) == False


def test_create_table(get_engine, create_table):

    create_table

    assert get_engine.dialect.has_table(get_engine, 'tweets') == True


def test_save(get_connection, create_insert):

    ins, tweets = create_insert

    result = get_connection.execute(ins)

    assert result.inserted_primary_key == [1]


def test_select(create_table, get_connection, create_insert):

    ins, tweets = create_insert

    result = get_connection.execute(ins)

    s = select([tweets])

    result = get_connection.execute(s)

    row = result.fetchone()

    assert row['title'] == 'people'


def test_select_where(create_insert, get_connection):

    ins, tweets = create_insert

    get_connection.execute(ins)

    s = select([tweets]).where(tweets.c.author == 'Ozan')

    result = get_connection.execute(s)

    row = result.fetchone()

    assert row['title'] == 'people'


def test_update(create_insert, get_connection):

    ins, tweets = create_insert

    get_connection.execute(ins)

    upd = update(tweets).where(tweets.c.author == 'Ozan'). \
        values(image='one picture')

    get_connection.execute(upd)

    sel = select([tweets]).where(tweets.c.image == 'one picture')
    result = get_connection.execute(sel)

    row = result.fetchone()

    assert row['image'] == 'one picture'


def test_save_all(create_table, get_engine):
    """ testing bulk inserts """

    tweet_list = []
    tweet1 = create_tweet_class()

    tweet2 = create_tweet_class()

    tweet3 = create_tweet_class()


    tweet_list.append(vars(tweet1))
    tweet_list.append(vars(tweet2))
    tweet_list.append(vars(tweet3))

    tweets = create_table

    engine = get_engine
    conn = engine.connect()

    # insert multiple data
    res_ins = conn.execute(tweets.insert(), tweet_list)

    assert res_ins.is_insert == True

    # selecting rows to validate
    sel = select([tweets]).where(tweets.c.author == 'Ozan')
    res = conn.execute(sel)

    row = res.lastrowid

    assert row == 3


def test_initialize_database(create_table, get_engine):

    create_table

    assert get_engine.dialect.has_table(get_engine, 'TWEETS') == True


def test_get_connection(get_engine):

    engine = get_engine
    conn = engine.connect()

    assert type(conn) == sqlalchemy.engine.Connection

def test_select_all(get_engine, create_insert, get_config):

    ins, tweets = create_insert

    get_engine.connect().execute(ins)

    tweets = select_all(get_config, True)

    assert len(tweets) == 1


def deleted_all(create_insert, get_engine, get_config):
    ins, tweets = create_insert

    get_engine.connect().execute(ins)

    tweets = select_all(get_config)

    assert len(tweets) == 1

    del_st = tweets.delete()

    get_engine.connect().execute(del_st)

    assert len(tweets) == 0


def test_export_to_csv(get_engine, create_insert, get_config):

    ins, tweets = create_insert

    get_engine.connect().execute(ins)

    tweets = select_all(get_config, True)
    filename = 'mycsvfile.csv'
    dict_list = [vars(tweet) for tweet in tweets]

    keys = dict_list[0].keys()
    with open(filename, 'w', encoding='utf-8') as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(dict_list)


    df = pd.read_csv(filename)

    assert len(tweets) == len(df)
