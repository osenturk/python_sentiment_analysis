""" tests for collect news """

from datetime import datetime, timedelta

from utils.collect_news import get_news
from utils.collect_news import save_news
from utils.collect_news import get_news_by_sources
from utils.tweet_utils import json_news_to_object
from utils.tweet_utils import get_sources
from utils.collect_news import filter_google_news
from utils.collect_news import add_sentiment
from utils.dao_layer import select_all


def test_get_google_news(get_config):

    cfg = get_config

    last_date = str(datetime.now())[0:10]
    first_date = str(datetime.today() - timedelta(days=cfg['google_news']['criteria']['news_age']))[0:10]

    keyword = 'amazon'

    google_news = get_news(first_date, last_date, keyword, cfg)

    assert len(google_news) >= 2


def test_filter_google_news(get_config):

    cfg = get_config

    last_date = str(datetime.now())[0:10]
    first_date = str(datetime.today() - timedelta(days=cfg['google_news']['criteria']['news_age']))[0:10]

    keyword = 'amazon'
    limit = 2

    google_news = get_news(first_date, last_date, keyword, cfg)
    tweet_news = [json_news_to_object(g_news, keyword) for g_news in google_news]
    
    # google_filtered_news = filter(lambda x: x., tweet_news)

    google_news = filter_google_news(tweet_news, limit)

    assert len(google_news[0:limit]) == limit


def test_get_google_news_by_sources(get_config):

    cfg = get_config
    limit = cfg['rest']['news_limit']

    last_date = str(datetime.now())[0:10]
    first_date = str(datetime.today() - timedelta(days=cfg['google_news']['criteria']['news_age']))[0:10]

    keyword = 'amazon'
    sources = get_sources(cfg)

    google_news = get_news_by_sources(first_date, last_date, keyword, cfg, sources)

    assert len(google_news) >= 2


def test_save_news(get_config):

    save_news(get_config, True)

    tweets = select_all(get_config, True)
    assert len(tweets) >= 0

def test_add_sentiment(get_config):

    save_news(get_config, True)
    tweets = select_all(get_config, True)

    add_sentiment(get_config,tweets)

    tweets = select_all(get_config, True)
    assert len(tweets) >= 0
