""" testing util class """


import pytest

from utils.tweet_utils import get_sentiment
from utils.tweet_utils import create_tweet_class


# @pytest.mark.skip(reason="too much time consuming")
# def test_find_similarities():
#     """ find similarities with spacy package: text analytics"""
#
#     NLP = spacy.load('en')
#     print("corpus was loaded...")
#
#     keywords = ['cat', 'dog', 'banana']
#
#     assert find_similarities(keywords[0], keywords[1], NLP) > 0.5
#
#     assert find_similarities(keywords[0], keywords[2], NLP) < 0.5
#
#     assert find_similarities(keywords[1], keywords[2], NLP) < 0.5


def test_json_news_to_object():

    tweet = create_tweet_class()

    assert tweet.author == 'Ozan'
    assert tweet.title == 'title'
    assert tweet.desc == 'desc'


def test_get_sentiment():

    text = "Uber Is One Of The Worst Performing IPOs Ever"
    score = get_sentiment(text)

    assert score < 0


def test_sort_tweet_sent():

    tweet1 = create_tweet_class()
    tweet1.p_desc = -0.55
    tweet1.keyword = 'Amazon'
    tweet2 = create_tweet_class()
    tweet2.p_desc = 0.45
    tweet2.keyword = 'Amazon'
    tweet3 = create_tweet_class()
    tweet3.keyword = 'Amazon'
    tweet3.p_desc = 0.35
    tweet4 = create_tweet_class()
    tweet4.keyword = 'Uber'
    tweet4.p_desc = 0.95

    news_list = []
    news_list.append(tweet1)
    news_list.append(tweet2)
    news_list.append(tweet3)
    news_list.append(tweet4)

    sent_list=sorted(news_list, key=lambda tweet: (tweet.keyword, -tweet.p_desc))



